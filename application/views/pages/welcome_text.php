<div class="page-header">
    <h1 class="">App Welcome Text</h1>
</div>
<br>
<?php if (!empty($_SESSION['success'])): ?>
    <div class="alert alert-success" role="alert"><?= $_SESSION['success'] ?></div>
    <?php unset($_SESSION['success']) ?>
    <?php endif; ?>
    <form action="<?= base_url('app/welcome_text') ?>" method="post">
        <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <textarea class="form-control" rows="20" required="" name="welcome_text"><?= $result['value'] ?></textarea>
        <br>
        <div>
            <button class="btn btn-primary btn-lg center-block" type="submit">Update</button>
        </div>
    </form>