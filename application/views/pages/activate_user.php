<div class="container">
    <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="https://trimaxmowers.co.uk/wp-content/uploads/2017/02/Trimax_logo_header.png">
        <hr>
        <h1>User Active !</h1>
    </div>

    <div class="row">
        <div class="col-md-6 offset-md-3">
            <h4 class="mb-3">User Details</h4>
            <p><strong>First Name</strong>: <?= $first_name ?></p>
            <?php if (!empty($last_name)): ?><p><strong>Last Name</strong>: <?= $last_name ?></p> <?php endif; ?>
            <?php if (!empty($company_name)): ?><p><strong>Company Name</strong>: <?= $company_name ?></p><?php endif; ?>
            <p><strong>Email Address</strong>: <a href="mailto:<?= $email ?>"><?= $email ?></a></p>
            <?php if (!empty($phone)): ?><p><strong>Phone number</strong>: <?= $phone ?></p><?php endif; ?>
            <?php if (!empty($remark)): ?><p style="white-space: pre-line"><strong>Remark</strong>: <?= $remark ?></p><?php endif; ?>
        </div>
    </div>

</div>