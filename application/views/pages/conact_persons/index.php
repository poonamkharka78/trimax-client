<div class="container-fluid">
    <div class="page-header clearfix">
        <h1 class="pull-left" style="margin: 0">Contact Persons</h1>
        <a href="<?= base_url('app/contact_add') ?>" class="btn btn-sm btn-primary pull-right">Add Person</a>
    </div>
    <br>
    <?php if (!empty($_SESSION['success'])): ?>
    <div class="alert alert-success" role="alert"><?= $_SESSION['success'] ?></div>
        <?php unset($_SESSION['success']) ?>
    <?php endif; ?>
    <div class="table-responsive">
        <table class="table table-striped table-bordered" id="example">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($result as $value): ?>
                <tr>
                        <td><?= $value['name'] ?></td>
                        <td><?= $value['description'] ?></td>
                        <td><?= $value['phone'] ?></td>
                        <td><a href="mailto:<?= $value['email'] ?>"><?= $value['email'] ?></a></td>
                        <td>
                            <?php if (!empty($value['image'])): ?>
                                <img src="<?= base_url($value['image']) ?>" class="img-responsive" style="height: 50px" />
                            <?php endif; ?>
                        </td>
                            <td>
                                    <a href="<?= base_url('app/contact_edit/' . $value['id']) ?>" class="btn btn-sm btn-warning">Edit Person</a>
                                    <a href="<?= base_url('app/contact_delete/' . $value['id']) ?>" class="btn btn-sm btn-danger" onclick="return confirm('Delete Person?')">Delete Person</a>
                            </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>