<div class="container-fluid">
    <div class="page-header">
        <h1 class="">Edit Contact Person</h1>
    </div> 
    <br>
    <form action="<?= base_url('app/contact_edit/' . $result['id']) ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" required="" name="name" value="<?= $result['name'] ?>"/>
        </div>
        <div class="form-group">
            <label>Description</label>
            <textarea class="form-control" name="description"><?= $result['description'] ?></textarea>
        </div>
        <div class="form-group">
            <label>Phone</label>
            <input type="text" class="form-control" required="" name="phone" value="<?= $result['phone'] ?>"/>
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" required="" name="email" value="<?= $result['email'] ?>"/>
        </div>
        <div class="form-group">
            <label>Image</label>
            <input type="file" name="userfile" accept="image/*" id="imgInp2" />
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
                <?php if (!empty($result['image'])): ?>
                    <p>Old Image</p>
                    <p><img src="<?= base_url($result['image']) ?>" class="img-responsive" id="blah" /></p>
                        <p class="text-center"><a href="<?= base_url('app/contact_remove_image/' . $result['id']) ?>" onclick="return confirm('Remove This Image?')" class="btn btn-danger">Remove This Image</a></p>
                    <?php endif; ?>
            </div>
            <div class="col-md-6">
                <p>New Image Preview</p>
                <p><img src="" class="img-responsive" id="blah2" /></p>
            </div>
        </div>
    </form>
</div>