<div class="container-fluid">
    <div class="page-header">
        <h1 class="">New Contact Person</h1>
    </div> 
    <br>
    <form action="<?= base_url('app/contact_add') ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" required="" name="name" value=""/>
        </div>
        <div class="form-group">
            <label>Description</label>
            <textarea class="form-control" name="description"></textarea>
        </div>
        <div class="form-group">
            <label>Phone</label>
            <input type="text" class="form-control" required="" name="phone" value=""/>
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" required="" name="email" value=""/>
        </div>
        <div class="form-group">
            <label>Image</label>
            <input type="file" name="userfile" accept="image/*" id="imgInp" />
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        <p><img src="<?= base_url($result['image']) ?>" id="blah" class="img-responsive" /></p>
    </form>
</div>