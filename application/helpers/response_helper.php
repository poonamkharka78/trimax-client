<?php

if (!isset($CI)) {
    $CI = get_instance();
}

/**
 * Format JSON For Standard Format
 * @param $data
 * @param $status
 * @return json
 */
function formatJson($data) {
    $ci = get_instance();
    $ci->output->set_content_type('application/json', 'UTF-8');
    return json_encode(['data' => $data, 'message' => null, 'status' => 1], JSON_NUMERIC_CHECK);
}

/**
 * Format JSON For Standard Error Format
 * @param $message
 * @param $status
 * @return json
 */
function formatJsonError($message, $status = 2) {
    $ci = get_instance();
    $ci->output->set_content_type('application/json', 'UTF-8');
    return json_encode(['data' => null, 'message' => $message, 'status' => $status], JSON_NUMERIC_CHECK);
}

/**
 * Upload File
 * @param String $file userfile
 * @param String $postkey 'img_url'
 * @param String $upload_folder current class
 * @param String $allowed_types 'gif|jpg|png|jpeg'
 */
function upload($file = 'userfile', $postkey = 'img_url', $upload_folder = NULL, $allowed_types = 'gif|jpg|png|jpeg') {
    $CI = & get_instance();
    $config['upload_path'] = 'uploads/' . (empty($upload_folder) ? $CI->router->class : $upload_folder) . '/';
    $config['allowed_types'] = $allowed_types;
    $config['encrypt_name'] = TRUE;
    $CI->load->library('upload');
    $CI->upload->initialize($config);
    $rtn = ['status' => '', 'error' => ''];
    if ($CI->upload->do_upload($file)) {
        $rtn['status'] = TRUE;
        $_POST[$postkey] = 'uploads/' . (empty($upload_folder) ? $CI->router->class : $upload_folder) . '/' . $CI->upload->data('file_name');
    } else {
        $rtn['status'] = FALSE;
        $rtn['error'] = $CI->upload->display_errors();
    }

    return $rtn;
}

/**
 * Database Table
 *
 * @author Milan Sanathara
 */
class table {

    static function __callStatic($table, $arguments = NULL) {
        global $CI;
        return $CI->db->get_where($table, empty($arguments) ? NULL : $arguments[0])->result_array();
    }

    /**
     * @param string $table Table Name
     * @param string $column Table Column
     * @param mixed $where CI Where Condition
     * @param mixed $order_by field name
     * @param mixed $order ASC OR DESC
     */
    static function column($table, $column, $where = NULL, $order_by = NULL, $order = 'ASC', $group_by = NULL) {
        global $CI;
        if (!empty($order_by)) {
            $CI->db->order_by($order_by, $order);
        }
        if (!empty($group_by)) {
            $CI->db->group_by($group_by);
        }
        $result = $CI->db->get_where($table, $where)->result_array();
        $new_result = array();
        foreach ($result as $item) {
            $new_result[] = $item[$column];
        }
        return $new_result;
    }

    /**
     * @param string $table Table Name
     * @param string $key Table Column Name
     * @param string $value Table Column Name
     * @param mixed $where CI Where Condition
     * @param mixed $order_by field name
     * @param mixed $order ASC OR DESC
     */
    static function key_value($table, $key, $value, $where = NULL, $order_by = NULL, $order = 'ASC') {
        global $CI;
        if (!empty($order_by)) {
            $CI->db->order_by($order_by, $order);
        }
        $result = $CI->db->get_where($table, $where)->result_array();
        $new_result = array();
        foreach ($result as $item) {
            $new_result[$item[$key]] = $item[$value];
        }
        return $new_result;
    }

    static function insert($table, $data) {
        /* @var $CI MY_Model */
        global $CI;
        $CI->db->insert($table, $data);
        return $CI->db->insert_id();
    }

    static function update($table, $data, $where) {
        /* @var $CI MY_Model */
        global $CI;
        $CI->db->update($table, $data, $where);
        return $CI->db->affected_rows();
    }

    static function delete($table, $where) {
        /* @var $CI MY_Model */
        global $CI;
        $CI->db->delete($table, $where);
        return $CI->db->affected_rows();
    }

}

function uniqidReal($lenght = 13) {
    // uniqid gives 13 chars, but you could adjust it to your needs.
    if (function_exists("random_bytes")) {
        $bytes = random_bytes(ceil($lenght / 2));
    } elseif (function_exists("openssl_random_pseudo_bytes")) {
        $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
    } else {
        throw new Exception("no cryptographically secure random function available");
    }
    return substr(bin2hex($bytes), 0, $lenght);
}
