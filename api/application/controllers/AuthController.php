<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property User $user
 */
class AuthController extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user');
    }

    public function signup() {
        $this->validation();
        if ($this->form_validation->run()) {
            if (!empty($_FILES['userfile']['name'])) {
                $result = upload('userfile', 'profile_img', 'profile');
                if ($result['status'] == FALSE) {
                    $errors = $this->upload->error_msg;
                    echo formatJsonError(implode("\n", $errors));
                    return;
                }
            }
            $user_id = $this->user->insert($_POST);
            $user = $this->user->getUser($user_id);
            $config['protocol'] = 'mail';
            $config['mailtype'] = 'html';

            $this->load->library('email');
            $this->email->initialize($config);
            /*
             * User Mail
             */
            $this->email->from('app@trimaxmowers.co.uk', 'Trimax');
            $this->email->to($_POST['email']);
            $this->email->subject('Trimax Account');
            $this->email->message($this->load->view('emails/register', $_POST, true));
            $this->email->send();
            /*
             * Admin Mail
             */
//            $this->email->to('maulik.php5626@gmail.com');
//            $this->email->cc('bhutkevin@gmail.com'); 
            $this->email->to('info@trimaxmowers.co.uk');
            $this->email->from('app@trimaxmowers.co.uk', 'Trimax');
            $this->email->subject('New User Register');
            $data = elements(array('first_name', 'last_name', 'email', 'phone', 'company_name', 'remark'), $_POST);
            $data['url'] = base_url('login/activate_user?user=' . base64_encode($user['id']));
            $this->email->message($this->load->view('emails/new_register_info', $data, true));
            $this->email->send();
            echo formatJson($user);
        } else {
            $errors = $this->form_validation->error_array();
            echo formatJsonError(implode("\n", $errors));
        }
    }

    private function validation() {
        $this->form_validation->set_rules('first_name', 'first name', 'required');
        $this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
    }

    public function login() {
        $result = table::users(array('email' => $_POST['email'], 'password' => sha1($_POST['password'])));
        if (!empty($result)) {
            $user = $this->user->getUser($result[0]['id']);
            if ($user['status'] == 'inactive') {
                echo formatJsonError("Welcome to Trimax family. Your registration request received successfully. You will get notified for the approval status.", 3);
            } else {
                echo formatJson($user);
            }
        } else {
            echo formatJsonError("Invalid Login Details");
        }
    }

    public function resetpassword_request() {
        $result = table::users(array('email' => $_POST['email']));
        if (!empty($result)) {
            $data = (object) $_POST;
            $code = uniqidReal(4);
            // send email
            $message = <<<MAIL
Hi,{$data->email}

Code: $code

Enter above code in app for password reset.

From Trimax App.
MAIL;
            mail($data->email, "Reset Password Code", $message);
            echo formatJson(['code' => $code]);
        } else {
            echo formatJsonError("This email is not registered with our app");
        }
    }

    public function resetpassword() {
        table::update('users', array('password' => sha1($_POST['password'])), array('email' => $_POST['email']));
        echo formatJson(null);
    }

}
