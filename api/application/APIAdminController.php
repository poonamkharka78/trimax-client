<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

class APIAdminController extends Controller
{
    private $loginKey = '4FIuX2Qpd2eOPFL1SAEk';

    public function users()
    {
        return view('api_admin.iframe', ['url' => "https://api.trimaxmowers.co.uk/users?LOGIN_KEY={$this->loginKey}"]);
    }
    public function registrationRequests()
    {
        return view('api_admin.iframe', ['url' => "https://api.trimaxmowers.co.uk/users/inactive?LOGIN_KEY={$this->loginKey}"]);
    }
    public function contactpersons()
    {
        return view('api_admin.iframe', ['url' => "https://api.trimaxmowers.co.uk/app/contactpersons?LOGIN_KEY={$this->loginKey}"]);
    }

    public function welcomeText()
    {
        return view('api_admin.iframe', ['url' => "https://api.trimaxmowers.co.uk/app/welcome_text?LOGIN_KEY={$this->loginKey}"]);
    }

}
