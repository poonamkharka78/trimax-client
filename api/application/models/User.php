<?php

class User extends CI_Model {

    function insert($data) {
        $insertData = elements(array('first_name', 'last_name', 'email', 'password', 'phone', 'company_name', 'country_code', 'country_name', 'profile_img', 'remark'), $data);
        $insertData['created_at'] = date('Y-m-d H:i:s', now());
        $insertData['password'] = sha1($insertData['password']);
        return table::insert('users', $insertData);
    }

    function getUser($user_id) {
        $result = $this->db->get_where('users', array('id' => $user_id))->row_array();
        if (!empty($result['profile_img'])) {
            $result['profile_img'] = base_url($result['profile_img']);
        }
        return $result;
    }

    function profileUpdate($id, $data) {
        $updatetData = elements(array('first_name', 'last_name', 'email', 'password', 'phone', 'company_name', 'country_code', 'country_name', 'profile_img'), $data);
        $finalData = array();
        if (!empty($updatetData['password'])) {
            $updatetData['password'] = sha1($updatetData['password']);
        }
        foreach ($updatetData as $key => $value) {
            if (!empty($value)) {
                $finalData[$key] = $value;
            }
        }
        table::update('users', $finalData, array('id' => $id));
    }

}
